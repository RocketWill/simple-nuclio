###
 # @Date: 2021-11-21 11:01:29
 # @LastEditors: will.chengyong
 # @LastEditTime: 2021-12-04 21:30:08
 # @FilePath: /general/nuclio/helloworld/run.sh
### 
docker run \
  -it \
  --volume /Users/will/workspace/luokung/general/nuclio/helloworld/function.yaml:/etc/nuclio/config/processor/processor.yaml \
  --mount type=bind,source=/Users/will/workspace/luokung/general/nuclio/helloworld/helloworld.py,target=/opt/nuclio/helloworld.py \
  --name my-function \
  --publish 8090:8080 \
  -v /Users/will/workspace/luokung/general/nuclio/helloworld:/outputs \
  my-hello:0.0.1 bash

 