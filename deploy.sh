###
 # @Date: 2021-11-21 13:39:46
 # @LastEditors: will.chengyong
 # @LastEditTime: 2021-12-04 23:55:20
 # @FilePath: /general/nuclio/helloworld/deploy.sh
### 
source ~/bin/.bash_profile

nuctl deploy hello-world2 --run-image my-hello:1.0.0 \
    --runtime python \
    --handler helloworld:handler \
    --platform local \
    --volume /Users/will/workspace/luokung/general/nuclio/helloworld/helloworld.py:/opt/nuclio/helloworld.py