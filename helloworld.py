'''
Date: 2021-11-21 10:56:56
LastEditors: will.chengyong
LastEditTime: 2021-12-05 14:20:51
FilePath: /general/nuclio/helloworld/helloworld.py
'''

import json
import numpy as np
import cv2
import time

class RequestParser:
    def parse_boundary(self, content_type):
        if isinstance(content_type, str):
            boundary = '--' + content_type.replace('multipart/form-data; boundary=', '')
            boundary = boundary.encode()
        elif isinstance(content_type, bytes):
            boundary = b'--' + content_type.replace(b'multipart/form-data; boundary=', b'')
        else:
            raise ValueError("Invalid content_type type.")
        return boundary

    def parse_image_meta(self, image_meta_data):
        image_meta_data = image_meta_data.split(b'\r\n\r\n')
        image_meta_data = [ele for ele in image_meta_data if not b'Content-Disposition' in ele]
        assert len(image_meta_data) == 1, "Length of image meta data contents should be 1."
        image_meta_data = image_meta_data[0].replace(b'\r\n', b'')
        return json.loads(image_meta_data.decode('utf-8'))
    
    def parse_image_data(self, image_data, image_meta):
        image_info = image_meta["image"]
        h, w, _ = np.int0([image_info["height"], image_info["width"], image_info["channel"]])
        image_data = image_data.split(b'\r\n\r\n')
        image_data = [ele for ele in image_data if not ele is b'' and not b'form-data' in ele and not b'stream' in ele]
        assert len(image_data) == 1, "Length of image data data contents should be 1."
        image_data = image_data[0].rsplit(b'\r\n', 1)[0] # cause' there are many '\r\n' in image data, so we just remove the last
        image = None
        for c in [3, 4]:
            try:
                image = np.fromstring(image_data, np.uint8).reshape(h, w, c) 
            except Exception as e:
                print(e)
        return cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # BGR to RGB

    def get_image_meta_and_data(self, event):
        content_type = event.content_type
        boundary = self.parse_boundary(content_type)
        form_data = event.body
        content = form_data.split(boundary)
        content = [ele for ele in content if b'form-data' in ele]
        assert len(content) == 2, "Length of content not match."
        meta_data, image_data = content[0], content[1]
        image_meta = self.parse_image_meta(meta_data)
        image = self.parse_image_data(image_data, image_meta)
        return image_meta, image
    
    def __call__(self, event):
        return self.get_image_meta_and_data(event)
        
def init_context(context):
    context.logger.info("Init context...  0%")
    # model = ModelHandler()
    # context.user_data.model = model
    context.request_parser = RequestParser()
    context.logger.info("Init context...100%")


def handler(context, event):
    # context.logger.info_with('Invoked', method=event.method)
    image_meta, image = context.request_parser(event)
    cv2.imwrite("/outputs/{}.png".format(time.time()), image)
    return context.Response(body=image_meta,
                            headers=None,
                            content_type='application/json',
                            status_code=200)

