###
 # @Date: 2021-12-05 00:09:03
 # @LastEditors: will.chengyong
 # @LastEditTime: 2021-12-05 00:10:25
 # @FilePath: /general/nuclio/helloworld/start.sh
### 

# launch dashboard
docker run -p 8070:8070 -v /var/run/docker.sock:/var/run/docker.sock -v /tmp:/tmp --name nuclio-dashboard quay.io/nuclio/dashboard:stable-amd64

# build docker
docker build --tag my-function:latest .

# use curl
curl --location --request POST 'localhost:8070/api/function_invocations' \
--header 'x-nuclio-function-name: helloworld' \
--header 'x-nuclio-function-namespace: nuclio' \
--header 'x-nuclio-log-level: debug' 